package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class ResultsPage {

    @FindBy(id = "sortDropdown")
    private WebElement sortDropdownButton;

    @FindBy(className = "full-bold")
    private List<WebElement> flightPrices;

    public ResultsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getSortDropdownButton() { return sortDropdownButton; }

    public List<WebElement> getFlightPrices() { return flightPrices; }

    public List<Integer> getPrices() {
        List<Integer> res=new ArrayList<>();
        for (WebElement element :flightPrices) {
            String price = element.getText().replaceAll("\\D+","");
            System.out.println("Prices: "+price);
            res.add(Integer.valueOf(price));
        }
        return res;
    }
}
